<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Thanks again! Now go create something AMAZING! :D
-->



<!-- PROJECT SHIELDS -->
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
![License](https://img.shields.io/badge/dynamic/json.svg?label=License&url=https://gitlab.com/api/v4/projects/24010546?license=true&query=license.name&colorB=yellow)
![Created at](https://img.shields.io/badge/dynamic/json.svg?label=Created%20at&url=https://gitlab.com/api/v4/projects/24010546&query=created_at&colorB=informational)
![Last activity](https://img.shields.io/badge/dynamic/json.svg?label=Last%20activity&url=https://gitlab.com/api/v4/projects/24010546&query=last_activity_at&colorB=informational)
[![Starrers](https://badgen.net/gitlab/stars/infinite-labs/private-payments)](https://gitlab.com/infinite-labs/private-payments/-/starrers/)
[![Forks](https://badgen.net/gitlab/forks/infinite-labs/private-payments)](https://gitlab.com/infinite-labs/private-payments/-/forks/)

[![LinkedIn](https://img.shields.io/badge/-LinkedIn-black.svg?logo=linkedin&colorB=555)](https://www.linkedin.com/company/infinitelabs-co/)
[![Twitter](https://img.shields.io/badge/-Twitter-black.svg?logo=twitter&colorB=555)](https://www.twitter.com/labsinfinite)
[![Web](https://img.shields.io/badge/-Web-black.svg?logo=firefox&colorB=555)](https://infinitelabs.co/)


<!-- PROJECT LOGO -->
<br />
<p align="center">
  <a href="https://gitlab.com/infinite-labs/private-payments">
    PRIVATE PAYMENTS
  </a>

  <h1 align="center">Keep your data private</h1>

  <p align="center">
    Welcome to our project
    <br />
    <strong>Want to know more? Explore our documents »</strong>
    <br />
<ol>
<li><a href="https://docs.google.com/presentation/d/e/2PACX-1vTLV317ZsY-OFCQ1i4K79TaxYBQFPY_RkzIBSO4IAZY_kWQjXZIA3-q5WXyU4AhZrYjLzSQwWuxVr2p/pub?start=false&loop=false&delayms=3000">Presentation</a></li>
<li><a href="https://gitlab.com/infinite-labs/private-payments/issues">Report Bug</a></li>
<li><a href="https://gitlab.com/infinite-labs/private-payments/issues">Request Feature</a></li>
</ol>    
    
  </p>
</p>



<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary>
  Table of Contents
  </summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgements">Acknowledgements</a></li>
  </ol>
</details>



<!-- ABOUT THE PROJECT -->

## About The Project

### What is the content creators problem?

The global economic landscape is changing. Digital payments have already replaced cash in many countries, and while central banks across the world are being questioned, it is becoming clear how blockchain is rebuilding the global economy. Blockchain technology is not only playing a pivotal role in reestablishing the current finance infrastructure, but also helping to redefine how we transact with each other and how we view money.

While the public transparency of blockchains is a desirable feature, it becomes a concern when dealing with privacy. Blockchain transactions are traceable and irreversible, which means they remain permanently traceable to the associated addresses. Although addresses are anonymous, privacy of transactions is at higher risk that with traditional banking, as if an individual gets ever linked to its address, privacy of all movements associated to that account is totally lost.

Although it may seem that guaranteeing public transparency and privacy is incompatible, it is actually possible with a cryptographic technique called Zero-Knowledge Proof of Knowledge (ZKPoK). A ZKPoK is a method by which one party (the prover) can convince to another party (the verifier) that some proposition is valid, whilst revealing nothing more than the validity of such a proposition. That is, with ZKPoKs it is possible to prove the veracity of logic statements that concern private data. In the blockchain context, ZKPoKs make possible to verify that transactions have been computed correctly without revealing any details of the trade, such as where the transactions originated from, where it went or how much was transferred.

A particular type of ZKPoK, called zero-knowledge Succinct Non-interactive Argument of Knowledge (zk-SNARK), happens to be a possible solution to these issues. This ZKPoK satisfies two crucial properties: the size of the proofs is small and the verification process is very fast. This way, it is possible to create scalable blockchains that either require only constant amount of data to verify its current state or are able to handle tones of transactions by doing them off-chain and only storing small proofs of the transactions on-chain.

The main goal of the proposal is the development of a new generation payment system with taxation based on a public blockchain and on ZKPoK.
In particular, using circuit-based zero knowledge proofs. Although there is some study of this type of circuits in other fields, it is still to
explore if the results apply in the context of zero-knowledge cryptography. We expect that the complex requirements of the circuits
needed to implement the taxation system will lead this research to consider other possible encoding of circuits or using different classes
of polynomials.

### Built With

This section should list any major frameworks that you built your project using. Leave any add-ons/plugins for the acknowledgements section. Here are a few examples.
* [Bootstrap](https://getbootstrap.com)
* [JQuery](https://jquery.com)
* [Laravel](https://laravel.com)



<!-- GETTING STARTED -->
## Getting Started

This is an example of how you may give instructions on setting up your project locally.
To get a local copy up and running follow these simple example steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* npm
  ```sh
  npm install npm@latest -g
  ```

### Installation

1. Get a free API Key at [https://example.com](https://example.com)
2. Clone the repo
   ```sh
   git clone https://github.com/your_username_/Project-Name.git
   ```
3. Install NPM packages
   ```sh
   npm install
   ```
4. Enter your API in `config.js`
   ```JS
   const API_KEY = 'ENTER YOUR API';
   ```



<!-- USAGE EXAMPLES -->
## Usage

Use this space to show useful examples of how a project can be used. Additional screenshots, code examples and demos work well in this space. You may also link to more resources.

_For more examples, please refer to the [Documentation](https://example.com)_



<!-- ROADMAP -->
## Roadmap

See the [open issues](https://gitlab.com/infinite-labs/moncon-framework/issues) for a list of proposed features (and known issues).



<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

Distributed under the MIT License. See `LICENSE` for more information.



<!-- CONTACT -->
## Contact

Your Name - [@your_twitter](https://twitter.com/your_username) - email@example.com

Project Link: [https://github.com/your_username/repo_name](https://github.com/your_username/repo_name)



<!-- ACKNOWLEDGEMENTS -->
## Acknowledgements
* [GitHub Emoji Cheat Sheet](https://www.webpagefx.com/tools/emoji-cheat-sheet)
* [Img Shields](https://shields.io)
* [Choose an Open Source License](https://choosealicense.com)
* [GitHub Pages](https://pages.github.com)
* [Animate.css](https://daneden.github.io/animate.css)
* [Loaders.css](https://connoratherton.com/loaders)
* [Slick Carousel](https://kenwheeler.github.io/slick)
* [Smooth Scroll](https://github.com/cferdinandi/smooth-scroll)
* [Sticky Kit](http://leafo.net/sticky-kit)
* [JVectorMap](http://jvectormap.com)
* [Font Awesome](https://fontawesome.com)
